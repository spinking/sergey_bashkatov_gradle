package studio.eyesthetics.sergey_bashkatov_gradle

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import studio.eyesthetics.feature1.Feature1
import studio.eyesthetics.feature2.Feature2

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val feature1 = Feature1()
        val feature2 = Feature2()

        feature1.printFeatureName()
        feature2.printFeatureName()
    }
}