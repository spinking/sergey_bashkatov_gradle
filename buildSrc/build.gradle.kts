plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    google()
    gradlePluginPortal()
}

gradlePlugin {
    plugins {
        register("printer-plugin") {
            id = "printer-plugin"
            implementationClass = "studio.eyesthetics.sergey_bashkatov_gradle.plugins.PrinterPlugin"
        }
        register("android-app-plugin") {
            id = "android-app-plugin"
            implementationClass = "studio.eyesthetics.sergey_bashkatov_gradle.plugins.AndroidAppPlugin"
        }

        register("android-library-plugin") {
            id = "android-library-plugin"
            implementationClass = "studio.eyesthetics.sergey_bashkatov_gradle.plugins.AndroidLibraryPlugin"
        }

        register("generator-plugin") {
            id = "generator-plugin"
            implementationClass = "studio.eyesthetics.sergey_bashkatov_gradle.plugins.GeneratorPlugin"
        }
    }
}

dependencies {
    implementation("com.github.lalyos:jfiglet:0.0.9")
    implementation("com.android.tools.build:gradle:7.2.2")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
}