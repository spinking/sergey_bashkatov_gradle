package studio.eyesthetics.sergey_bashkatov_gradle.plugins

import com.android.build.gradle.LibraryExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.getByType
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

class AndroidLibraryPlugin : Plugin<Project> {
    override fun apply(target: Project) = target.applyLibrary()

    private fun Project.applyLibrary() {
        plugins.run {
            apply("com.android.library")
            apply("kotlin-android")
        }

        extensions.getByType<LibraryExtension>().run {
            compileSdk = Versions.compileSdkVersion

            defaultConfig {
                targetSdk = Versions.compileSdkVersion
                minSdk = Versions.minSdkVersion
            }

            compileOptions {
                sourceCompatibility = JavaVersion.VERSION_1_8
                targetCompatibility = JavaVersion.VERSION_1_8
            }

            val action: KotlinJvmOptions.() -> Unit = { jvmTarget = JavaVersion.VERSION_1_8.toString() }
            (this as ExtensionAware).extensions.configure(CONFIGURE_TYPE, action)
        }
    }

    companion object {
        private const val CONFIGURE_TYPE = "kotlinOptions"
    }
}