package studio.eyesthetics.sergey_bashkatov_gradle.plugins

object AppDetails {
    const val applicationId = "studio.eyesthetics.sergey_bashkatov_gradle"
    const val versionCode = 1
    const val versionName = "1.0.0"
}

object Versions {
    const val compileSdkVersion = 33
    const val minSdkVersion = 23
}

object Default {
    const val groupName = "myGroup"
}