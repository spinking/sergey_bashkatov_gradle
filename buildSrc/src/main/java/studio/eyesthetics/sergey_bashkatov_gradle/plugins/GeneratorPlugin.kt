package studio.eyesthetics.sergey_bashkatov_gradle.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project

class GeneratorPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        val generationTask = target.tasks.register(TASK_NAME, GenerationTask::class.java) {
            this.group = Default.groupName
            this.execute()
        }

        target.tasks.whenTaskAdded {
            if (this.name == FINALIZED_TASK)
                this.finalizedBy(generationTask)
        }
    }

    companion object {
        private const val TASK_NAME = "generator"
        private const val FINALIZED_TASK = "preBuild"
    }
}