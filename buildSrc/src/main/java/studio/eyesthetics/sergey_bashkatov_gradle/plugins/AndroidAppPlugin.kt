package studio.eyesthetics.sergey_bashkatov_gradle.plugins

import com.android.build.gradle.AppExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.getByType
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

class AndroidAppPlugin : Plugin<Project> {
    override fun apply(target: Project) = target.applyAndroid()

    private fun Project.applyAndroid() {
        plugins.run {
            apply("com.android.application")
            apply("kotlin-android")
        }

        extensions.getByType<AppExtension>().run {
            compileSdkVersion(Versions.compileSdkVersion)

            defaultConfig {
                applicationId = AppDetails.applicationId
                minSdk = Versions.minSdkVersion
                targetSdk = Versions.compileSdkVersion
                versionCode = AppDetails.versionCode
                versionName = AppDetails.versionName
            }

            compileOptions {
                sourceCompatibility = JavaVersion.VERSION_1_8
                targetCompatibility = JavaVersion.VERSION_1_8
            }

            val action: KotlinJvmOptions.() -> Unit = { jvmTarget = JavaVersion.VERSION_1_8.toString() }
            (this as ExtensionAware).extensions.configure(CONFIGURE_TYPE, action)
        }
    }

    companion object {
        private const val CONFIGURE_TYPE = "kotlinOptions"
    }
}