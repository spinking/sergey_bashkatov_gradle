package studio.eyesthetics.sergey_bashkatov_gradle.plugins

import org.gradle.api.DefaultTask
import org.gradle.api.file.Directory
import org.gradle.api.tasks.TaskAction

abstract class GenerationTask : DefaultTask() {
    private val directory: Directory
        get() = project.layout.projectDirectory.dir(PATH)

    @TaskAction
    fun execute() {
        val fileName = "$DEFAULT_FILE_NAME.kt"
        val content = getFileContent()
        directory.apply {
            asFile.mkdirs()
            file(fileName).asFile.writeText(content)
        }
    }

    private fun getFileContent(): String {
        return """
            package $PACKAGE
            
            class $DEFAULT_FILE_NAME {
                fun hello() {
                    println("Hello, World!")
                }
            }
        """.trimIndent()
    }

    companion object {
        private const val PATH = "src/main/java/studio/eyesthetics/sergey_bashkatov_gradle"
        private const val PACKAGE = "studio.eyesthetics.sergey_bashkatov_gradle"
        private const val DEFAULT_FILE_NAME = "Product"
    }
}