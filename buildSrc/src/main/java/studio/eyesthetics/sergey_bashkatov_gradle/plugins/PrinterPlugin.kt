package studio.eyesthetics.sergey_bashkatov_gradle.plugins

import com.github.lalyos.jfiglet.FigletFont
import org.gradle.api.Plugin
import org.gradle.api.Project

class PrinterPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        val figletTask = target.tasks.register(PRINT_TASK_NAME) {
            group = Default.groupName
            doLast {
                val nice = FigletFont.convertOneLine(GRAFFITI_TEXT)
                println(nice)
            }
        }
        target.tasks.whenTaskAdded {
            if (this.name == FINALIZED_TASK)
                this.finalizedBy(figletTask)
        }
    }

    companion object {
        private const val PRINT_TASK_NAME = "print"
        private const val GRAFFITI_TEXT = "Do it only when task added!"
        private const val FINALIZED_TASK = "assembleDebug"
    }
}